# Actors Show

The Actors Show is a simple application that show a list of actors took from a public API. 

How it run?

The application is compatible with iOS 9.0 onwards and it can run on all devices compatible with iOS 9.0 onwards. 
In the first view you will find a table with all actors. It is refreshing when you run the app so, you don’t need to press any buttons to show the list. If you want to see more about an actor you can select one by clicking on a cell. A new view will be showing with the actor description. It is easy and useful. 

What I used? 
For handling the API and parsing the data I used Alamofire which is a third party library. For downloading the images I used Alamofire ( and alamofireImage) and NSData. It is accompanied with a few tests(unit tests and UI tests).

What is next?
Build a better UI. I am planning to work more on this app using all data from api, creating a flashy UI using others third party libraries. 

Author: Daniel Leah
