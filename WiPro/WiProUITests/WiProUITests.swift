//
//  WiProUITests.swift
//  WiProUITests
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import XCTest

class WiProUITests: XCTestCase {
    
    var vcMain : UIViewController!
   
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") 
        vcMain = vc
        
        _ = vcMain.view
        
        
        continueAfterFailure = false
        XCUIApplication().launch()


    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
       
    }
    
}
