//
//  Actor.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import Foundation
//I made a struct Actor that help me to save data
struct Actor {
    
    let name: String
    let description: String
    let dob: String
    let country: String
    let image: String
    
    init?(json: JSON)
    {
        guard let name = json["name"] as? String,
        let description = json["description"] as? String,
        let dob = json["dob"] as? String,
        let country = json["country"] as? String,
        let image = json["image"] as? String
            else{return nil}
        self.name = name
        self.description = description
        self.dob = dob
        self.country = country
        self.image = image
    }
}
