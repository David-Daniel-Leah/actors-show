//
//  ActorsClientGet.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import UIKit
import Alamofire

typealias JSON = [String: Any]

class ActorsClient {
    
    static let shared = ActorsClient()
    private init(){}
    //getting the data from JSON using Alamofire. I needed to add the domain to the ATS
    func fetchActors(succes successBlock : @escaping (GetActorsRespons) -> Void)
    {
        Alamofire.request("http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors").responseJSON { response in
            guard let json = response.result.value as? JSON else { return}
            do{
                let getActor = try GetActorsRespons(json: json)
                successBlock(getActor)
            } catch{}
            }
        
    }
}
