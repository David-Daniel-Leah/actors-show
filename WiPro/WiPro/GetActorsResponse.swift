//
//  GetActorsResponse.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import Foundation

struct GetActorsRespons {
    let actors: [Actor]
    //Get the actors from JSON.
    init (json : JSON) throws {
        guard let result = json["actors"] as? [JSON] else {throw NetworkingError.badNetworking}
        let actors = result.map{ Actor(json: $0)}.flatMap{ $0 }
        self.actors = actors
    }
}
