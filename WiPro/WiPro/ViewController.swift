//
//  ViewController.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//
//import the library
import UIKit
import AlamofireImage
import Alamofire
class ViewController: UIViewController {

    @IBOutlet var viewModel : ViewModel!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.masksToBounds = true
        tableView.layer.borderColor = UIColor.black.cgColor
        tableView.layer.borderWidth = 1.0
    //calling the view model for getting the data from JSON
        viewModel.fetchActors{
            self.tableView.reloadData()
            }
    }
    //function that sent to the detail view the actor
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard segue.identifier == "DetailVC",
            let detailVC = segue.destination as? DetailsVC
            else { return }
        
        guard let actor = sender as AnyObject as? Actor else {return}
            detailVC.actor = actor
    }
}

//table view data source extension
extension ViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) ->Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.separatorStyle = .none
        return viewModel.numberOfItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        configureCell(cell: cell, forRowInIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell: UITableViewCell, forRowInIndexPath indexPath: IndexPath){
        cell.textLabel?.text = "  "+viewModel.titleForItemAtIndexPath(indexPath: indexPath)
        cell.detailTextLabel?.text = "   "+viewModel.subtitleForItemAtIndexPath(indexPath: indexPath)
        Alamofire.request(viewModel.imageForItemInIndexPath(indexPath: indexPath)).responseImage { (response) in
            if let image = response.result.value{
                    cell.imageView?.image = image
            }
        }
       
        
    }
}
//the delegate for passing the data
extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "DetailVC", sender: viewModel.actorForSegue(indexPath: indexPath))
    }
    
}


