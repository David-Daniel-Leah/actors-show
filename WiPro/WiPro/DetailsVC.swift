//
//  DetailsVC.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import UIKit
//Details view
class DetailsVC: UIViewController {

    var actor: Actor!
   
    @IBOutlet weak var imageActor: UIImageView!
    @IBOutlet weak var nameActor: UILabel!
    @IBOutlet weak var dobActor: UILabel!
    @IBOutlet weak var countryActor: UILabel!
    @IBOutlet weak var descriptionActor: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameActor.text = actor.name
        self.dobActor.text = actor.dob
        self.countryActor.text = actor.country
        self.descriptionActor.text = actor.description
        guard let url = URL(string: actor.image) else {return}
        guard let data = NSData(contentsOf: url) else {return}
        DispatchQueue.main.async {
            self.imageActor?.image = UIImage(data: data as Data)
        }
    }
    
}
