//
//  ActorsClient.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import UIKit
import Alamofire

typealias JSON = [String: Any]

class ActorsClient: NSObject {
    
    static let shared = ActorsClient()
    private override init(){}
 
    func fetchActors(succes successBlock : @escaping (GetActorsRespons) -> Void)
    {
        Alamofire.request("http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors").responseJSON { response in
            guard let json = response.result.value as? JSON else { return}
            do{
                let getActor = try GetActorsRespons(json: json)
                successBlock(getActor)
            } catch{}
        }.resume()
        
    }
}
