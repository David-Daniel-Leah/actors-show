//
//  ViewModel.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import UIKit

// the ViewModel Class (MVVM)
class ViewModel: NSObject {
    var actors = [Actor]()
    
    func fetchActors(completion: @escaping () -> ()) {
        ActorsClient.shared.fetchActors { (response) in
            self.actors = response.actors
            completion()
        }
    }
    
    func numberOfItemsInSection(section : Int) -> Int{
        return actors.count
    }
    
    func titleForItemAtIndexPath(indexPath: IndexPath) -> String{
        return actors[indexPath.row].name
    }
    
    func subtitleForItemAtIndexPath(indexPath: IndexPath) -> String{
        return actors[indexPath.row].country
    }
    
    func imageForItemInIndexPath(indexPath: IndexPath) ->URL{
         let url = URL(string: actors[indexPath.row].image)
        return url!
    }
    
    func actorForSegue(indexPath: IndexPath) -> Actor{
        return actors[indexPath.row]
    }
    

}
