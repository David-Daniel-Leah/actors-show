//
//  NetworkingError.swift
//  WiPro
//
//  Created by Daniel Leah on 28/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import Foundation
//an error class
enum NetworkingError: Error
{
    case badNetworking
}
