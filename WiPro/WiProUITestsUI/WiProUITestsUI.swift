//
//  WiProUITestsUI.swift
//  WiProUITestsUI
//
//  Created by Daniel Leah on 29/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import XCTest

class WiProUITestsUI: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.cells.containing(.staticText, identifier:"Tom Cruise").staticTexts["United States"].tap()
        
        let actorsButton = app.navigationBars["WiPro.DetailsVC"].buttons["Actors"]
        actorsButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Will Smith"]/*[[".cells.staticTexts[\"Will Smith\"]",".staticTexts[\"Will Smith\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        actorsButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Johnny Depp"]/*[[".cells.staticTexts[\"Johnny Depp\"]",".staticTexts[\"Johnny Depp\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        actorsButton.tap()

    }
    
}
