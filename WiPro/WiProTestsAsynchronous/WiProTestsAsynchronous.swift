//
//  WiProTestsAsynchronous.swift
//  WiProTestsAsynchronous
//
//  Created by Daniel Leah on 29/11/2017.
//  Copyright © 2017 Daniel Leah. All rights reserved.
//

import XCTest
class WiProTestsAsynchronous: XCTestCase {
    var sessionUnderTest : URLSession!
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
    }
    
    
    
    
    override func tearDown() {
        sessionUnderTest = nil
        super.tearDown()
    }
    
    func testValidCallForDataGetCode200() {
        let url = URL(string: "http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors")
        
        let promise = expectation(description: "status code: 200")
        
        let dataTask = sessionUnderTest.dataTask(with: url!) { (data, response, error) in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            }else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200{
                    promise.fulfill()
                }else{
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
